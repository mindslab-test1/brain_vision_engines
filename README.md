# 실행 방법
## Docker-compose
```
# 서버 container일 경우 (ENTRYPOINT 지정해둔 경우)
docker-compose -f COMPOSE_FILE_PATH up -d
# 학습 및 전처리 container일 경우
docker-compose -f COMPOSE_FILE_PATH run -p EXTERNAL_PORT:DOCKER_PORT SERVICE_NAME bash
```